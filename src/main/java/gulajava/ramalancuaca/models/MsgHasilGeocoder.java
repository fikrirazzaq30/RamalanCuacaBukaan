package gulajava.ramalancuaca.models;

/**
 * Created by Gulajava Ministudio.
 */
public class MsgHasilGeocoder {

    private String alamatgabungan = "";
    private String alamatPendek = "";

    public MsgHasilGeocoder() {
    }

    public String getAlamatgabungan() {
        return alamatgabungan;
    }

    public void setAlamatgabungan(String alamatgabungan) {
        this.alamatgabungan = alamatgabungan;
    }

    public String getAlamatPendek() {
        return alamatPendek;
    }

    public void setAlamatPendek(String alamatPendek) {
        this.alamatPendek = alamatPendek;
    }
}
