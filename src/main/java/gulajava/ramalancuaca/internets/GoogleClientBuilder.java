package gulajava.ramalancuaca.internets;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Gulajava Ministudio.
 */
public class GoogleClientBuilder {

    //INISIALISASI GOOGLE API CLIENT
    public synchronized static GoogleApiClient getLocationClient(Context context,
                                                                 GoogleApiClient.ConnectionCallbacks connectionCallbacks,
                                                                 GoogleApiClient.OnConnectionFailedListener connectionFailedListener) {

        GoogleApiClient.Builder googleBuilder = new GoogleApiClient.Builder(context);
        googleBuilder.addConnectionCallbacks(connectionCallbacks);
        googleBuilder.addOnConnectionFailedListener(connectionFailedListener);
        googleBuilder.addApi(LocationServices.API);

        return googleBuilder.build();
    }


    //SETEL LOCATION REQUEST NETWORK
    public static LocationRequest getLocationNetworkReq() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);   // 10 detik
        locationRequest.setFastestInterval(5000); //5 detik
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        return locationRequest;
    }


    //SETEL LOCATION REQUEST GPS
    public static LocationRequest getLocationGPSReq() {

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);  //10 detik
        locationRequest.setFastestInterval(5000); //5 detik
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(10); //10 meter

        return locationRequest;
    }

}
