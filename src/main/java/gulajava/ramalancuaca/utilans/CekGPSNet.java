package gulajava.ramalancuaca.utilans;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by Gulajava Ministudio.
 */

public class CekGPSNet {


    private Context konteks = null;
    private LocationManager lokasimanager = null;
    private ConnectivityManager conmanager = null;
    private TelephonyManager mTelephonyManager = null;
    private NetworkInfo netinfo = null;

    private boolean isInternet = false;
    private boolean isNetworkNyala = false;
    private boolean isGPSNyala = false;

    private boolean statusNetwork = false;
    private boolean statusGPS = false;

    private int networkTipe = TelephonyManager.NETWORK_TYPE_UNKNOWN;
    private String networkOperator = "";


    public CekGPSNet(Context ctx) {

        this.konteks = ctx;

        isInternet = false;
        isNetworkNyala = false;
        isGPSNyala = false;

        statusGPS = false;
        statusNetwork = false;

        lokasimanager = (LocationManager) konteks.getSystemService(Context.LOCATION_SERVICE);
        conmanager = (ConnectivityManager) konteks.getSystemService(Context.CONNECTIVITY_SERVICE);
        mTelephonyManager = (TelephonyManager) konteks.getSystemService(Context.TELEPHONY_SERVICE);

    }


    //  CEK APAKAH STATUS INTERNET MENYALA ATAU TIDAK
    public boolean cekStatusInternet() {

        netinfo = conmanager.getActiveNetworkInfo();

        isInternet = netinfo != null && netinfo.isConnected();

        return isInternet;
    }


    //CEK STATUS NETWORK BTS GSM DAN SELULER
    public boolean cekStatusNetworkGSM() {

        try {

            networkTipe = mTelephonyManager.getNetworkType();
            networkOperator = mTelephonyManager.getNetworkOperator();

            isNetworkNyala = networkOperator != null && networkOperator.length() > 0 ||
                    networkTipe != TelephonyManager.NETWORK_TYPE_UNKNOWN;

        } catch (Exception ex) {
            isNetworkNyala = false;
        }

        return isNetworkNyala;
    }


    //  CEK APAKAH STATUS JARINGAN GSM HIDUP/TIDAK, DAN BISA DIAMBIL LOKASI LAT LONG DARI SANA
    public boolean cekStatusNetwork() {

        try {
            isNetworkNyala = lokasimanager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
            isNetworkNyala = false;
        }

        return isNetworkNyala;
    }


    //  CEK APAKAH STATUS GPS HIDUP/TIDAK, DAN BISA DIAMBIL LOKASI LAT LONG NYA
    public boolean cekStatusGPS() {

        try {
            isGPSNyala = lokasimanager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
            isGPSNyala = false;
        }

        return isGPSNyala;
    }


    //  SETEL STATUS JARINGAN GSM DAN INTERNET
    public boolean getKondisiNetwork(boolean isInternetNyambung, boolean isJaringanNyambung) {

        if (isInternetNyambung && isJaringanNyambung) {
            statusNetwork = true;
        } else if (!isInternetNyambung && !isJaringanNyambung) {
            statusNetwork = false;
        } else {
            statusNetwork = false;
        }

        return statusNetwork;
    }


    //  SETEL STATUS JARINGAN GPS
    public boolean getKondisiGPS(boolean isGPSnyambungs) {

        statusGPS = isGPSnyambungs;

        return statusGPS;
    }


}
