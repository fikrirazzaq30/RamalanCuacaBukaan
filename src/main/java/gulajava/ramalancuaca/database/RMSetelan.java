package gulajava.ramalancuaca.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Gulajava Ministudio.
 */
public class RMSetelan extends RealmObject {

    @PrimaryKey
    private int idData;
    private String strMsWaktuUpdate;
    private String namaLokasi;
    private String namaLokasiPanjang;
    private String strLatitude;
    private String strLongitude;
    private boolean notifikasiCuaca;

    public RMSetelan() {
    }

    public int getIdData() {
        return idData;
    }

    public void setIdData(int idData) {
        this.idData = idData;
    }


    public String getStrMsWaktuUpdate() {
        return strMsWaktuUpdate;
    }

    public void setStrMsWaktuUpdate(String strMsWaktuUpdate) {
        this.strMsWaktuUpdate = strMsWaktuUpdate;
    }


    public String getNamaLokasi() {
        return namaLokasi;
    }

    public void setNamaLokasi(String namaLokasi) {
        this.namaLokasi = namaLokasi;
    }

    public String getNamaLokasiPanjang() {
        return namaLokasiPanjang;
    }

    public void setNamaLokasiPanjang(String namaLokasiPanjang) {
        this.namaLokasiPanjang = namaLokasiPanjang;
    }

    public String getStrLatitude() {
        return strLatitude;
    }

    public void setStrLatitude(String strLatitude) {
        this.strLatitude = strLatitude;
    }

    public String getStrLongitude() {
        return strLongitude;
    }

    public void setStrLongitude(String strLongitude) {
        this.strLongitude = strLongitude;
    }


    public boolean isNotifikasiCuaca() {
        return notifikasiCuaca;
    }

    public void setNotifikasiCuaca(boolean notifikasiCuaca) {
        this.notifikasiCuaca = notifikasiCuaca;
    }


}
